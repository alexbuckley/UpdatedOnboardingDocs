.. include:: images.rst

Installer Perl Modules Installed
___________________________________

This screen tells you that the installer has all the dependencies it needs, to create the database in the next screen.

.. figure:: images/perl_modules_installed.*
   :alt: Perl modules installed

   Perl modules installed


Click on the *Continue to the next step* button to load the next stage of the web installer.
