.. include:: images.rst

Redirect to onboarding tool
__________________________________

If you wait for around 10 seconds this screen should redirect you to the onboarding tool start screen.

.. figure:: images/redirect.*
   :alt: redirect to  onboarding tool

   Redirect to onboarding tool


.. note::
  If after waiting you are not redirected select the link pointed out by the arrow in the above screenshot.
