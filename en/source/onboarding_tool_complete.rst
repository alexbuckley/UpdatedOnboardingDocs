.. include:: images.rst

Onboarding tool complete
____________________________

This page tells you if the circulation rule was created successfully, and that the Koha installation is complete

.. figure:: /images/onboarding_tool_complete.*
   :alt: Onboarding tool complete


1. **Web installer message:** Indicates if the web installation is complete
2. **Circulation rule creation message:** Indicates if the circulation rule was created successfully.
3. **Start using Koha:** Click to login to Koha using the Koha administrator account you created using the onboarding tool.


