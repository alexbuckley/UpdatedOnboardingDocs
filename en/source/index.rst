
Koha 17.05 Installation Documentation (en)
============================================

.. include:: images.rst
.. toctree::
   :maxdepth: 2
   :numbered:

   intro
   web_installer_login
   web_installer_start_screen
   web_installer_perl_modules
   web_installer_database_settings
   web_installer_ready_for_data
   web_installer_database_tables_created
   web_installer_install_basic_configurations
   web_installer_setup_marc_flavour
   web_installer_marc21_setup
   selected_data_added
   redirect_to_onboarding
   create_a_library
   create_patron_category_1
   create_patron
   create_item_type_1
   create_circulation_rule_1
   onboarding_tool_complete
   login
   staff_interface
   license
