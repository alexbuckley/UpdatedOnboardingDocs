.. include:: images.rst

Ready to fill tables with data
____________________________________

This screen is informing you everything is set up for you to create the database tables in the next screen.

.. figure:: images/ready_for_data.*
   :alt: Database ready for data

   Database ready for data

Click the *Continue to the next step* button to load the next stage

.. note::
  This step can take some time. Please be patient.
