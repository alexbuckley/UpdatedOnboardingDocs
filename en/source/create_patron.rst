.. include:: images.rst

Create a patron
___________________

A superlibrarian patron user is required to log into the Koha staff interface, once the onboarding process has been completed.

.. note:: It is very important to document the username and password created here as they are the account credentials required to login as an administrator (superlibrarian) of Koha.

You will need to input/select:

* Surname
* First Name
* Card number
* Library
* Patron category
* Username
* Password

.. figure:: images/create_patron_1.*
   :alt: Create a patron

   Create a patron

1. **Patron category creation message:** This indicates that if the patron category was created successfully.
2. **Surname:** Surname, or something descriptive
3. **First name:** First name, or something descriptive
4. **Card Number:** This number must be unique
5. *Library* dropdown box: If a library has been created using this onboarding tool, it will be the only option. Otherwise select a random library from the sample libraries installed by the web installer.
6. *Patron category* dropdown box: If a patron category was created using this onboarding tool, it will be the only option. Otherwise select the *Staff* patron category.
7. **Superlibrarian permission:** This non-editable setting allows access to all librarian and web based Koha administration tasks in the staff interface. This user is the most powerful user in any Koha, so protect the credentials well.
8. **Username:** The username to log into the staff interface and OPAC (Online Patron Access Catalogue) with.
9. **Password:** A password consisting of letters, numbers, and spaces only which is greater than 8 characters.
10. **Confirm password:** Repeat the above password again
11. Click the *Submit* button to create the patron account
12. **Path to create patron:** Patron's can be created or altered by going to Patrons->New patron
13. **Assign patron permissions:** After creating a patron go More->Set permissions to assign permissions.


For example:

.. figure:: images/create_patron_1_example.*
   :alt: Create a patron example

   Create a patron example
