.. include:: images.rst

Select MARC flavour
__________________________________

You must choose your MARC flavour (format you want the bibliographic (catalog) records to be stored in the database) on this screen:

.. figure::  images/select_basic_configurations.*
   :alt: Select MARC flavour

   Select MARC flavour

1. **Unimarc:** This is frequently used in European countries (except for the UK) such as Italy.

2. **MARC21:** Selected by default, as it is more commonly used globally than UNIMARC.

3. Click the *Continue to the next step* button to confirm your choices
