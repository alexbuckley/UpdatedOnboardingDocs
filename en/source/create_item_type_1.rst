.. include:: images.rst

Create a Item type
______________________

If you did not install sample item types in the web installer then this screen will be displayed.

As with the patron category the item type is basically a template which you use to make multiple items with common characteristics.

You need to input:

* Item type code
* Description

.. figure::  images/create_item_type_1.*
   :alt: Create item type

   Create item type

1. **Administrator account creation message:** Indicates if the administrator patron was created successfully
2. **Item type code:** Code consisting of up to 10 letters
3. **Description:** Sentence describing what the item type is.
4. **Path to create item type:** Item types can be created or altered by going to Administrator->Item types
5. Click the *Submit* button to create the item type

For example:

.. figure:: images/create_item_type_1_example.*
   :alt: Create item type example

   Create item type example


.. note:: The regular expression filtering Item Type code input will only accept letters.
