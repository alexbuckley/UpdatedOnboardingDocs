.. include:: images.rst

Ready for basic configurations
__________________________________

This screen leads onto installing basic configurations necessary to use Koha.

.. figure:: images/install_basic_configurations.*
   :alt: Ready for basic configurations

   Ready for basic configurations

Click the *Continue to the next step* button
