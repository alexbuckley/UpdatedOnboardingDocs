.. include:: images.rst

Web Installer login
====================

**Web Installer login**


This login screen is the first screen that appears when installing Koha.

.. figure:: images/installer_login.*
   :alt: Web installer login

   Web Installer login

1. **Koha version name**: This will display whatever Koha version number you are installing. 

2. **Username**: This is database administrator username which is *koha_kohadev* by default.

3. **Password**: This is the database administrator password which is *password* by default. 

4. *Log in*: Select this button to login 
