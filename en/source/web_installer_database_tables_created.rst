.. include:: images.rst

Database tables created
__________________________

.. figure:: images/database_tables_created.*
   :alt: Database tables created

   Database tables created

Click the *Continue to the next step* button
