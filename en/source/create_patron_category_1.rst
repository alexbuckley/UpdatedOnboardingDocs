.. include:: images.rst

Create a patron category
_____________________________

All patrons in Koha must have a patron category. The patron category is like a jelly mold; multiple individual patron accounts can share common characteristics, such as circulation rules, notice frequency, holds allowed, and much more. Koha requires at least one patron category in order to add patrons. 

A patron category requires:

  * Category code
  * Description
  * Overdue notice required
  * Category type
  * Default privacy
  * Enrolment period: In months OR Until date

.. figure:: /images/create_patron_categories_1.*
   :alt: Create a patron category

   Create a patron category

1. **Library creation message:** Indicates whether the library was created successfully

2. **Category code:** Code consisting of up to 10 letters.

3. **Description:** Sentence describing what the patron category is.

4. *Overdue notice required* dropdown button: Set by default to 'No'. This specifies if you want the patron category to receive overdue notices.

5. **Category type:** This makes the category created a staff member.

6. **Default privacy:** Set by default to 'Default'. The Default privacy controls the amount of time that the reading history of the patron is stored for.

7. **Enrolment period-In months:** This is the number of months that the patrons created from this patron category are enrolled for.

8. **Enrolment period-Until date:** Select a date from the interactive datepicker calendar icon which appears when you click on this input box. The date you choose will be the enrolment end date for patrons created from this patron category.

9. **Path to create patron category:** More patron categories can be created or altered by going to Administration -> Patron categories

10. Click the *Submit* button to create the patron category.

For example:

.. figure:: /images/create_patron_category_1_example.*
   :alt: Create a patron category example

   Create a patron category example
