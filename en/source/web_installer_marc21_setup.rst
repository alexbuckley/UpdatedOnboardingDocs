.. include:: images.rst

MARC21 and UNIMARC setup
============================

.. note:: In the previous screen you either choose MARC21 or UNIMARC as your MARC flavour. This page outlines both.

MARC21 setup
_______________


.. figure:: images/marc21_basic_setup.*
   :alt: MARC21 setup

   MARC21 setup

1. **Default MARC21 Standard Authority Types:** An authority type is a template of an authority record (stores the search fields used to search an institutions catalog for an item). This option will install sample authority types.

2. **Default MACR21 bibliographic framework:** Following on from the MARC21 selection in the previous screen selecting this checkbox (and the *Import* button) will actually setup the MARC21 bibliographic framework as the desired MARC flavour.

3. **Selected matching rules for MARC21 bibliographic records:** The MARC21 matching rules are used to find all the information about specific bibliographic records using the authority type search values.

4. **'FA', a 'Fast Add' minimal MARC21 framework suitable for ILL, or on the fly cataloging:** The FA MARC framework displays fewer fields, and allows for faster cataloguing on the fly. Select this checkbox to install this minimal framework.

5. **Sample MARC21 bibliographic frameworks for some common types of bibliographic material:** This installs the template you will use to create items.

6. **Default Koha system authorised values:** This installs mandatory authorised values that every Koha installation needs.

7. **Default classification sources and filling rules:** This will insert several common library classification sources such as dewey decimal.

8. **Defines default message transports for email and sms:** This provides the option to the user of being able to change how the library contacts them. They can choose from the following: email address, printed message, sms, or phone call. Notices will not work without these.

9. **Patron attributes:** This does not install any data, and it needs to be removed.

10. **Sample frequencies for subscriptions:** For items that arrive on a regular and predictable basis (e.g. monthly magazines), installing sample frequencies is useful because you can simply select a pre-installed sample frequency when you create the item.

11. **Sample notices:** This inserts sample notices into the database, meaning all you have to do is customize these sample notices before you use them. Creating these by hand can be very time consuming and error prone.

12. **Defines default messages for the enhanced messaging configuration:** The enhanced messaging configuration lets the user choose what messages they receive from the institution. Selecting this will insert sample message types for the Due, Predue, and Advance notice message types.

13. **Defines default message transports for sending item due messages and advance notice messages through email and SMS:** This inserts the SMS and email transport types. Mandatory if you want to send notices by email or SMS.

14. **Sample numbering patterns for subscriptions:** This option will install sample numbering patterns, saving you having to manually insert them for magazines and other periodicals.

15. **Some basic default authorised values for library locations, item lost status, etc. You can change these at any time after installation:** This installs authorised values, which filter user inputs, to make sure they match specified criteria before the library or other object is created.

16. **CSV profiles:** Using Koha you can export items. The CSV profile defines how you want to export the item(s). CSV/Tab exports will not work without these.

17. **Coded values conforming to the Z39.71-2006 holdings statements for bibliographic items:** Not required to use Koha. A holding statement tells the user if the library has a specific item and where it is located. Selecting will install sample code values for items in the holding statements.

18. **MARC code list for relators:** Not required to use Koha. A relator is a person who was invloved in the development of the item (e.g. author). Selecting this will insert sample relator records into the database.

19. **Some basic currencies with USA dollar as default for ACQ module:** Install US dollar, Great British Pound, Canadian Dollar and Euro currencies. Set the US dollar as the default acquisition currency. Not installing these will mean that a currency will need to be defined before Acquisitions can be used.

20. **Useful patron attribute types:** Not required to use Koha. Patrons have a barcode which identifies them. By clicking this checkbox your making sure that this patron barcode is displayed on the patron summary screen.

21. **Sample patron types and categories:** Not required to use Koha. Selecting this will install sample patron categories into the database. This will mean the Create patron category step in the onboarding tool is skipped.

22. **Sample label and patron card data:** Not required to use Koha, but recommended for most new installs. Selecting this will install sample data for labels and patron cards and will make it easier to create a labels and patron cards in the future.

23. **Sample holidays: Sunday, Christmas, New Year’s** Not required to use Koha. Selecting this will insert sample repeatable public holidays (e.g. Christmas) into the database.

24. **A set of default item types:** Not required for Koha. Selecting this will insert sample item type data into the database, meaning Create Item type step in the onboarding tool will be skipped.

25. **Sample Libraries:** Not required for Koha. Selecting this will insert sample libraries into the database. Developers will often want this for demo or development purposes. Selecting this means the Create library step in the onboarding tool will be skipped.

26. **Sample news items:** Not required for Koha. Selecting this will insert sample news items that are displayed on the OPAC (Online Public Access Catalog) and staff client into the database.

27. **Sample patrons:** Not required for Koha. Patrons are the people who use the Koha Library Management System, for example: library users and staff. Selecting this will insert a number of sample patrons into the database. Developers will often want this for demo or development purposes. 

28. **Sample quotes:** Not required for Koha. Quotes of the day can be displayed to library users on the OPAC (Online Public Access Catalog) interface. Selecting this will install a few library related sample quotes.

29. **Allow access to the following servers to search and download record information:** Not required to use Koha, but recommended for most new installs. Selecting this configures Koha to allow access to a selection of Z39.50 targets, which allow searching the target servers for MARC21 records to import them into your Koha instance. This can avoid the need to manually input all record information manually, or greatly speed up the process.

30. Click the *Import* button to install the default and selected data values.



Unimarc setup
__________________________________


There are only 2 data values in the Unimarc advanced setup which are unique:

* Authority Structure for UNIMARC in English
* Default UNIMARC bibliographic framework in English


.. note::  Optional values are the same for both Unimarc and MARC21, therefore their definition will not be repeated.

.. figure:: /images/unimarc_basic_setup.*
     :alt: Unimarc setup

     Unimarc setup

1. *Authority structure for UNIMARC in English:* Authority records store the search terms used to find items in the database. Unlike MARC21, when using Unimarc the authority structure needs to be compatible with Unimarc. Selecting this option makes it compatible.

2. *Default UNIMARC bibliographic framework in English:* In the previous screen you clicked to use the Unimarc flavour, now by selecting this option you are confirming you want to use that framework.

3. Click the *Import* button to install the sample data
