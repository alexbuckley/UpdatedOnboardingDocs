.. include:: images.rst



Selected data added
____________________

After you have clicked the *Import* button in either the MARC21 or UNIMARC setup screen this screen will appear. It shows you if the selected/default  data values were successfully inserted into the database.

.. figure::  images/data_added_1.*
   :alt: Selected data added

   Selected data added

1. **Optional data added:** If you selected an optional data value then it will be displayed here. If you did not choose any optional data value then the **optional data added** title will not be displayed.

2. **Mysql data added:** These data values will always be installed and consequently displayed on this screen.

3. **Mandatory data added:** Same as above.

4. **Installation message:** Tells you if the Koha database was successfully created ready for you to use the onboarding tool.

5. Click the *Set up some of Koha's basic requirements* button to go to a redirection screen, which in turn will take you to the onboarding tool.
