.. |installer_login| image:: /images/installer_login.*
.. |create library 1| image:: /images/create_library_screen_1.*
.. |create library 1 ex| image:: /images/create_library_screen_1_example.*
.. |create patron cat 1| image:: /images/create_patron_categories_1.*
.. |create patron cat 1 ex| image:: /images/create_patron_category_1_example.*
.. |create patron 1| image:: images/create_patron_1.*
.. |create patron 1 ex| image:: images/create_patron_1_example.*
.. |create item type 1| image:: images/create_item_type_1.*
.. |create item type 1 example| image:: images/create_item_type_1_example.*
.. |create circulation rule 1| image:: images/create_circulation_rule_1.*
.. |onboarding tool complete| image:: images/onboarding_tool_complete.*
.. |login| image:: images/login.*
.. |login example| image:: images/login_example.*
.. |staff interface| image:: images/staff_interface.*
.. |web installer start screen| image:: images/installer_start_screen.*
.. |perl_modules_installed| image:: images/perl_modules_installed.*
.. |database settings| image:: images/database_settings.*
.. |connection established| image:: images/connection_established.*
.. |ready for data| image:: images/ready_for_data.*
.. |database tables created| image:: images/database_tables_created.*
.. |ready for basic configurations| image:: images/install_basic_configurations.*
.. |select setup marc flavour| image:: images/select_basic_configurations.*
.. |marc21 basic setup| image:: images/marc21_basic_setup.*
.. |marc21 advanced setup| image:: images/marc21_advanced_setup.*
.. |unimarc basic setup| image:: images/unimarc_basic_setup.*
.. |data added| image:: images/data_added_1.*
.. |redirect| image:: images/redirect.*
