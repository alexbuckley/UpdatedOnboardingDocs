.. include:: images.rst

Database Settings
__________________________

This screen informs you of the database settings and asks you to confirm them by clicking the *Continue to the next step* button.


.. note::
  For the majority of users these settings should be correct, if not then you should contact your support provider.

.. figure::  images/database_settings.*
   :alt: Database settings

   Database settings


1. **Database Settings:** Check these database settings are correct.
2. Click the *Continue to the next step* button to confirm the database settings

----------------------------------------------------------------------------------------

**Connection Established**


After you click the *Next* button the database connection is confirmed:

.. figure:: images/connection_established.*
   :alt: Connection established

   Database connection established

1. **Connection established message:** This informs you that the database has been successfully created.
2. Click the *Continue to the next step* button to load the next stage

.. Note:: If you have database connection difficulties here, the installer will not proceed. Double check the connection information in your koha-conf.xml file, and verify that the permissions and credentials on the database itself are correct.
