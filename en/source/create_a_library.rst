.. include:: images.rst

Create a library
___________________
The installer will prompt to create the first library or branch if the sample libraries were not installed.

A library in Koha is the digital representation of a "physical" place. More libraries can be added later by going to Administration -> Libraries and groups.

.. figure:: /images/create_library_screen_1.*
   :alt: Create library

   Create library


1. **Library code:** code consisting of up to 10 letters.
2. **Name:** Official name of the library, as it is commonly known.
3. **Create more libraries:** If more libraries are required, or changes need to be made to this freshly created library, go to Administration->Libraries and groups
4. Click the *Submit* button to create a library.


For example:

.. figure:: /images/create_library_screen_1_example.*
   :alt: Create library example

   Create library example


**Library code:** The regular expression that filters the acceptable inputs for library code only accepts up to 10 letters.

