.. include:: images.rst

Create a Circulation Rule
______________________________

This screen will always be displayed no matter what sample data you installed with the web installer.

Here you are creating a rule which applies the pre-defined organisational decisions of your library as to what restrictions you place on patrons borrowing items.

.. figure:: images/create_circulation_rule_1.*
   :alt: Create circulation rule

   Create circulation rule


.. note::
  Numbers are the only valid input for all input fields in this form.

1. **Item type creation message:** Indicates if the item type was created successfully
2. *Library branch* dropdown box: This is the library (or libraries) you want to apply the circulation rule to. By default it is set to *All*, however you can select a singular library to apply the rule to from the dropdown box.
3. *Patron categories* dropdown box: This is the patron categories you want to apply the circulation rule to. As with *Library branch* it is set to *All* by default but more options are avaliable.
4. *Item type* dropdown box: This is the item types that you want the circulation rule to apply to. Again more options are avaliable than the default selected *All* option.
5. **Current checkouts allowed:** This is the number of items allowed from the selected library, for selected patron categories and of the selected item type. Set to 50 by default.
6. **Loan period:** Number of days or hours that an item is allowed out for. Set to 14 by default.
7. *Units* dropdown box: Set by default to *Days*, the unit selecting in this field is applied to the numerical values written into **Loan Period** and **Renewals Period**
8. **Renewals Allowed:** Number of times a item can be renewed. Set to 10 by default.
9. **Renewals Period:** Number of days or hours that a renewal lasts for. Set to 14 by default.
10. *On shelf holds allowed* dropdown box: If items can be held whilst they are on the shelf.
11. **Path to create circulation rule:** Circulation rules can be created or altered by going to Administration -> Circulation and fine rules
12. Click the *Submit* button to create the circulation rule.


